const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n\n');
const { getLCM } = require('../utils/array');

class Monkey {
  constructor(lines, regexNumber, regexOperation) {
    this.lines = lines;
    this.regexNumber = regexNumber;
    this.regexOperation = regexOperation;
    this.items = this.lines[1].match(this.regexNumber).map((n) => parseInt(n));
    this.divisibleBy = parseInt(this.lines[3].match(this.regexNumber)[0]);
    this.inspectedItems = 0;
  }

  getWorryLevel(prev) {
    let { operator, operand } = this.lines[2].match(this.regexOperation).groups;
    operand = operand === 'old' ? prev : parseInt(operand);

    return (operator === '*')? prev * operand : prev + operand;
  }

  getNextMonkey(worryLevel) {
    const block = (worryLevel % this.divisibleBy === 0)? this.lines[4] : this.lines[5];
    return block.match(this.regexNumber)[0];
  }
}

const monkeys = content.map((data) => {
  const lines = data.trim().split('\n');
  const regexNumber = /\d+/g;
  const regexOperation = /new = old (?<operator>[\*\+]) (?<operand>\d+|old)/;

  return new Monkey(lines, regexNumber, regexOperation);
});

function getMonkeyBusiness(rounds) {
  const lcm = getLCM(monkeys.map((m) => m.divisibleBy));

  for (let i = 0; i < rounds; i++) {
    monkeys.forEach(monkey => {
      monkey.items.forEach(item => {
        let worryLevel = monkey.getWorryLevel(item);
        worryLevel = worryLevel % lcm;
        const nextMonkey = monkey.getNextMonkey(worryLevel);

        monkeys[nextMonkey].items.push(worryLevel);
      });
      monkey.inspectedItems += monkey.items.length;
      monkey.items = [];
    });
  }

  const inspectedItems = monkeys.map((monkey) => monkey.inspectedItems).sort((a, b) => b - a);

  return inspectedItems[0] * inspectedItems[1];
}

console.log(getMonkeyBusiness(10000));

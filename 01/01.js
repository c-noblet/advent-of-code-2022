const readLine = require('readline');
const fs = require('fs');
const file = './input.txt';
const rl = readLine.createInterface({
  input : fs.createReadStream(file),
  output : process.stdout,
  terminal: false
});

const arr = [];
let sum = 0;

rl.on('line', (line) => {
  if (line == '') {
    arr.push(sum);
    sum = 0;
  } else {
    sum += parseInt(line, 10);
  }
}).on('close', () => {
  const max = arr.reduce((a, b) => Math.max(a, b));
  console.log(max);
});

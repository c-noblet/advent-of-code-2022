const readLine = require('readline');
const fs = require('fs');
const file = './input.txt';
const rl = readLine.createInterface({
  input : fs.createReadStream(file),
  output : process.stdout,
  terminal: false
});

let arr = [];
let sum = 0;

rl.on('line', (line) => {
  if (line == '') {
    arr.push(sum);
    sum = 0;
  } else {
    sum += parseInt(line, 10);
  }
}).on('close', () => {
  let total = 0;
  arr = arr.sort((a,b) => b-a);
  for (let i = 0; i < 3; i++) {
    total += arr[i];
  }
  console.log(total);
});

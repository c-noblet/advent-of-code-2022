const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n\n').map((block) => block.split('\n').map((line) => JSON.parse(line)));

function compareSides(left, right) {
  if (typeof left === 'number' && typeof right === 'number') {
    return left === right ? 0 : left < right ? -1 : +1;
  }

  if (typeof left === 'number' || typeof right === 'number') {
    const leftArr = Array.isArray(left)? left : [left];
    const rightArr = Array.isArray(right)? right : [right];
    return compareSides(leftArr, rightArr);
  }

  const min = Math.min(left.length, right.length);

  for (let i = 0; i < min; i++) {
    const result = compareSides(left[i], right[i]);

    if (result) return result;
  }

  return left.length - right.length;
}

function getDecoderKey(input) {
  const dividerPackets = [[[2]], [[6]]];
  const isDividerPacket = (packet) => {
    return dividerPackets.map((x) => JSON.stringify(x))
      .includes(JSON.stringify(packet));
  }
  const packets = [...input.flat(), ...dividerPackets].sort(
    compareSides,
  );

  return packets.reduce(
    (product, packet, i) =>
      isDividerPacket(packet) ? product * (i + 1) : product,
    1,
  );
}

console.log(getDecoderKey(content));

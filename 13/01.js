const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n\n').map((block) => block.split('\n').map((line) => JSON.parse(line)));

function compareSides(left, right) {
  if (typeof left === 'number' && typeof right === 'number') {
    return left === right ? 0 : left < right ? -1 : +1;
  }

  if (typeof left === 'number' || typeof right === 'number') {
    const leftArr = Array.isArray(left)? left : [left];
    const rightArr = Array.isArray(right)? right : [right];
    return compareSides(leftArr, rightArr);
  }

  const min = Math.min(left.length, right.length);

  for (let i = 0; i < min; i++) {
    const result = compareSides(left[i], right[i]);

    if (result) return result;
  }

  return left.length - right.length;
}

function isInRightOrder(pair) {
  return compareSides(...pair) < 0;
}

function getSumOfIndices(input) {
  return input.reduce(
    (sum, pair, i) => (isInRightOrder(pair) ? sum + i + 1 : sum),
    0,
  );
}

console.log(getSumOfIndices(content));

const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n').map((r) => r.split('').map((t) => parseInt(t)));

const visibleTrees = [];

for (let i = 0; i < content.length; i++) {
  for (let j = 0; j < content[i].length; j++) {
    const tree = content[i][j];

    const leftTrees = content[i].slice(0, j);
    const leftVisibleTree = leftTrees.every(t => t < tree);

    const rightTrees = content[i].slice(j + 1);
    const rightVisibleTree = rightTrees.every(t => t < tree);
    
    const topTrees = content.slice(0, i).map((row) => row[j]);
    const topVisibleTree = topTrees.every(t => t < tree);
    
    const bottomTrees = content.slice(i + 1).map((row) => row[j]);
    const bottomVisibleTree = bottomTrees.every(t => t < tree);
    
    if (leftVisibleTree || rightVisibleTree || topVisibleTree || bottomVisibleTree) {
      visibleTrees.push(tree);
    }
  }
}

console.log(visibleTrees.length);

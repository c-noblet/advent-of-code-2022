const fs = require('fs');
const { getMax } = require('../utils/array');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n').map((r) => r.split('').map((t) => parseInt(t)));

const scores = [];

for (let i = 0; i < content.length; i++) {
  for (let j = 0; j < content[i].length; j++) {
    const currentTree = content[i][j];

    const leftTrees = content[i].slice(0, j).reverse();
    let leftCount = 0;
    for (const tree of leftTrees) {
      leftCount++;
      if (tree >= currentTree) break;
    }

    const rightTrees = content[i].slice(j + 1)
    let rightCount = 0;
    for (const tree of rightTrees) {
      rightCount++;
      if (tree >= currentTree) break;
    }

    const topTrees = content.slice(0, i).map((row) => row[j]).reverse();
    let topCount = 0;
    for (const tree of topTrees) {
      topCount++;
      if (tree >= currentTree) break;
    }

    const bottomTrees = content.slice(i + 1).map((row) => row[j]);
    let bottomCount = 0;
    for (const tree of bottomTrees) {
      bottomCount++;
      if (tree >= currentTree) break;
    }
    
    const result = leftCount * rightCount * topCount * bottomCount;
    scores.push(result);
  }
}

console.log(getMax(scores));

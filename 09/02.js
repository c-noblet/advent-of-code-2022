const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n').map((r) => r.split(' ')).map((t) => { return { direction:t[0], count: parseInt(t[1])}});

class Knot {
  constructor() {
    this.x = 0;
    this.y = 0;
  }

  move(direction) {
    if (direction === 'R') this.x++;
    if (direction === 'L') this.x--;
    if (direction === 'U') this.y--;
    if (direction === 'D') this.y++;
  }

  distanceFrom(knot) {
    return {
      x: knot.x - this.x,
      y: knot.y - this.y,
    };
  }

  shouldFollow(previousKnot) {
    const distance = this.distanceFrom(previousKnot);

    return Math.abs(distance.x) > 1 || Math.abs(distance.y) > 1;
  }
}

const numberOfKnots = 10;
const visitedPositions = new Set();
const rope = Array(numberOfKnots).fill().map(() => new Knot());

content.forEach((movement) => {
  for (let i = 0; i < movement.count; i++) {
    const head = rope[0];
    const tail = rope.at(-1);

    head.move(movement.direction);

    for (let j = 1; j < numberOfKnots; j++) {
      const currentKnot = rope[j];
      const previousKnot = rope[j - 1];
      const distance = currentKnot.distanceFrom(previousKnot);

      if (currentKnot.shouldFollow(previousKnot)) {
        currentKnot.x += Math.sign(distance.x);
        currentKnot.y += Math.sign(distance.y);
      }
    }

    visitedPositions.add(JSON.stringify(tail));
  }
});

console.log(visitedPositions.size);

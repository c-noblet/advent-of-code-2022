const fs = require('fs');
const file = './input.txt';
const lines = fs.readFileSync(file, 'utf-8').trim().split('\n');

const grid = [];

for (let i = 0; i < lines.length; i++)
  for (let j = 0; j < lines[i].length; j++)
    grid.push(lines[i][j].charCodeAt(0));

const start = grid.findIndex((i) => i === 'S'.charCodeAt(0));
const end = grid.findIndex((i) => i === 'E'.charCodeAt(0));

grid[start] = 'a'.charCodeAt(0);
grid[end] = 'z'.charCodeAt(0);

const bfs = (start) => {
  let queue = [[start, 0]];
  const cache = new Set([start]);
  while (queue.length > 0) {
    const [position, steps] = queue.shift();
    if (position === end) return steps;
    const arr = [
      lines[0].length + position,
      -lines[0].length + position,
      1 + position,
      -1 + position
    ];
    const results = arr.filter((r) => grid[r] <= grid[position] + 1 && !cache.has(r));
    results.forEach((r) => cache.add(r));
    queue = [...queue, ...results.map((c) => [c, steps + 1])];
  }
  return Infinity;
};

const result = grid.map((c, i) => ({ c, start: i }))
  .filter(({ c }) => c === 'a'.charCodeAt(0))
  .map(({ start }) => bfs(start))
  .reduce((min, v) => (min < v ? min : v));

console.log(result);

function sortArray(arr, order = 'asc') {
  return arr.sort((a, b) => (order === 'asc' ? a - b : b - a));
};

function sumArray(arr) {
  return  arr.reduce((acc, v) => acc + v, 0);
} 

function getMin(arr) {
  return Math.min(...arr);
};

function getMax(arr) {
  return Math.max(...arr);
};

function prefixArray(arr) {
  return arr.reduce(
    (prev, curr) => [
      ...prev,
      (prev[prev.length - 1] ? prev[prev.length - 1] + '/' : '') + curr,
    ],
    []
  );
}

function filterArray(arr) {
  return Array.from(new Set(arr.map(JSON.stringify))).map(JSON.parse);
}

function getLCM(arr) {
  const gcd = (a, b) => (b === 0 ? a : gcd(b, a % b));
  let lcm = 1;

  for (const divisor of arr) {
    lcm = (lcm * divisor) / gcd(lcm, divisor);
  }

  return lcm;
}

module.exports = {
  sortArray,
  sumArray,
  getMin,
  getMax,
  prefixArray,
  filterArray,
  getLCM
};

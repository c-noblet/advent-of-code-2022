const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('');

for (let i = 0; i < content.length; i++) {
  const nextChars = [];
    for (let j = 0; j < 14; j++) {
    nextChars.push(content[i - j] || null);
  }

  allDiferentLength = nextChars.filter((v,i) => nextChars.indexOf(v) === i).length;
  if (i >= 14 && allDiferentLength == nextChars.length) {
    console.log(i + 1);
    break;
  }
}

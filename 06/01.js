const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('');

for (let i = 0; i < content.length; i++) {
  const nextChars = [
    content[i - 3] || null,
    content[i - 2] || null,
    content[i - 1] || null,
    content[i],
  ]

  allDiferentLength = nextChars.filter((v,i) => nextChars.indexOf(v) === i).length;
  if (i >= 4 && allDiferentLength == nextChars.length) {
    console.log(i + 1);
    break;
  }
}

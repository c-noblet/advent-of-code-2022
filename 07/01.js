const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n');
const { sumArray, prefixArray } = require('../utils/array');

let path = [];
let dirs = {};

content.forEach((line) => {
  if (line === "$ cd /") path = [];
  if (line === "$ cd ..") path.pop();
  else if (line.includes('$ cd')) path.push(line.split('cd ')[1]);
  if (/\d/.test(line.split(' ')[0])) {
    const value = parseInt(line.split(' ')[0]);
    for (const dir of prefixArray(path)) {
      dirs[dir] = (dirs[dir] || 0) + value;
    }
  }  
});

dirs = Object.values(dirs);
dirs = dirs.filter((size) => size <= 100000);
const sum = sumArray(dirs);

console.log(sum);

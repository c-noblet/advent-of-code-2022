const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n');
const { prefixArray, getMin } = require('../utils/array');

let path = [];
let dirs = {};

content.forEach((line) => {
  if (line === "$ cd /") path = [];
  if (line === "$ cd ..") path.pop();
  else if (line.includes('$ cd')) path.push(line.split('cd ')[1]);
  if (/\d/.test(line.split(' ')[0])) {
    const value = parseInt(line.split(' ')[0]);
    for (const dir of prefixArray(path)) {
      dirs[dir] = (dirs[dir] || 0) + value;
    }
  }  
});

dirs = Object.values(dirs);
const min = getMin(dirs.filter((size) => size >= dirs[0] - (70000000 - 30000000)));
console.log(min);


const readLine = require('readline');
const fs = require('fs');
const file = './input.txt';
const rl = readLine.createInterface({
  input : fs.createReadStream(file),
  output : process.stdout,
  terminal: false
});

const opponentChoices = ['A', 'B', 'C'];
const myChoices = ['X', 'Y', 'Z'];

function getRoundResult(opponentChoice, myChoice) {
  const map = {};
  opponentChoices.forEach((choice, i) => {
    map[choice] = {};
    map[choice][myChoices[i]] = 3;
    map[choice][myChoices[(i + 1) % 3]] = 6;
    map[choice][myChoices[(i + 2) % 3]] = 0;
  });
  return map[opponentChoice][myChoice];
}

function getChoicePoint(myChoice) {
  if (myChoice == 'X') {
    return 1;
  } else if (myChoice == 'Y') {
    return 2;
  } else {
    return 3;
  }
}

let sum = 0;

rl.on('line', (line) => {
  const round = line.split(' ');
  const opponentChoice = round[0];
  const myChoice = round[1];
  const roundResult = getRoundResult(opponentChoice, myChoice);
  const choicePoint = getChoicePoint(myChoice);

  sum += roundResult + choicePoint;
}).on('close', () => {
  console.log(sum);
});

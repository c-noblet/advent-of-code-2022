
const readLine = require('readline');
const fs = require('fs');
const file = './input.txt';
const rl = readLine.createInterface({
  input : fs.createReadStream(file),
  output : process.stdout,
  terminal: false
});

const opponentChoices = ['A', 'B', 'C'];
const myChoices = ['A', 'B', 'C'];

function getRoundResult(opponentChoice, myChoice) {
  const map = {};
  opponentChoices.forEach((choice, i) => {
    map[choice] = {};
    map[choice][myChoices[i]] = 3;
    map[choice][myChoices[(i + 1) % 3]] = 6;
    map[choice][myChoices[(i + 2) % 3]] = 0;
  });
  return map[opponentChoice][myChoice];
}

function getChoicePoint(myChoice) {
  if (myChoice == 'A') {
    return 1;
  } else if (myChoice == 'B') {
    return 2;
  } else {
    return 3;
  }
}

function getMyChoice(opponentChoice, expectedResult) {
  const map = {};
  opponentChoices.forEach((choice, i) => {
    map[choice] = {};
    map[choice]['X'] = opponentChoices[i - 1] || 'C';
    map[choice]['Y'] = opponentChoices[i];
    map[choice]['Z'] = opponentChoices[i + 1] || 'A';
  });
  return map[opponentChoice][expectedResult];
}

let sum = 0;

rl.on('line', (line) => {
  const round = line.split(' ');
  const opponentChoice = round[0];
  const expectedResult = round[1];
  const myChoice = getMyChoice(opponentChoice, expectedResult);
  const choicePoint = getChoicePoint(myChoice);
  const roundResult = getRoundResult(opponentChoice, myChoice);
  
  sum += roundResult + choicePoint;
}).on('close', () => {
  console.log(sum);
});

const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n');

class Instruction {
  constructor(str) {
    if (str === 'noop') {
      this.name = 'noop';
      this.register = null;
      this.value = null;
    } else {
      const arr = str.split(' ');
      this.name = arr[0].slice(0, -1);
      this.register = arr[0].slice(-1);
      this.value = parseInt(arr[1]) || null;
    }
    this.end = null;
  }

  run(tick) {
    if (this.name == 'add') {
      this.end = tick + 1;
    }
    if (this.name == 'noop') {
      this.end = tick;
    }
  }

  updateRegister(registers) {
    if (this.register && !(this.register in registers)) {
      registers[this.register] = 1;
    }
    if (this.name == 'add') {
      return this.add(registers);
    }
    if (this.name == 'noop') {
      return this.noop();
    }
  }
  
  add(registers) {
    registers[this.register] += this.value; 
  }

  noop() {
  }
}

class Processor {
  constructor(instructions) {
    this.registers = {x:1};
    this.instructions = instructions;
    this.tick = 0;
    this.isAvailable = true;
    this.isRunning = true;
    this.currentInstruction = null;
    this.signalStrength = 0;
  }

  update() {
    this.tick++;
    if (this.tick % 40 === 20 || this.tick === 20) {
      this.signalStrength += this.tick * this.registers.x
      console.log(this.tick, this.registers, this.tick * this.registers.x);
    }
    if (this.instructions.length === 0 && this.isAvailable) {
      this.isRunning = false;
    } else {
      if(this.isAvailable) {
        this.currentInstruction = this.instructions.shift();
        this.isAvailable = false;
        this.currentInstruction.run(this.tick);
      }
      if (!this.isAvailable) {
        if (this.currentInstruction.end === this.tick) {
          this.currentInstruction.updateRegister(this.registers);
          this.isAvailable = true;
        }
      } 
      
    }
  }
}

const instructions = content.map(item => new Instruction(item));

const processor = new Processor(instructions);
while (processor.isRunning) {
  processor.update();
}

console.log(processor.registers, processor.signalStrength);

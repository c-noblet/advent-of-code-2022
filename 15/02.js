const fs = require('fs');
const { getMin, getMax } = require('../utils/array.js');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n');

const scans = content.map(l => {
  const match = l.match(/[-0-9]+/g).map(c => parseInt(c));
  return {
    sensor: { x: match[0], y: match[1] },
    beacon: { x: match[2], y: match[3] }
  }
});

function getLineIntersection(line1, line2) {
  const [x1, y1, x2, y2] = line1
  const [x3, y3, x4, y4] = line2

  if ((x1 === x2 && y1 === y2) || (x3 === x4 && y3 === y4)) return

  const denominator = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)

  if (denominator === 0) return

  const ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator
  const ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator

  if (ua < 0 || ua > 1 || ub < 0 || ub > 1) return

  const x = x1 + ua * (x2 - x1)
  const y = y1 + ua * (y2 - y1)

  return { x, y }
}

function getManhattanDistance(a, b) {
  return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
}

const beacons = scans.map(scan => scan.beacon);
const sensors = scans.map(scan => scan.sensor);
const manhattanDistances = scans.map(scan => getManhattanDistance(scan.sensor, scan.beacon));

const mins = sensors.map((sensor, index) => sensor.x - manhattanDistances[index]);
const maxs = sensors.map((sensor, index) => sensor.x + manhattanDistances[index]);
const minX = getMin(mins);
const maxX = getMax(maxs);

const sensorWithDistances = []
const sensorBoundaries = []

for (const scan of scans) {
  const { sensor, beacon } = scan;
  const distance = getManhattanDistance(
    sensor,
    beacon
  );

  sensorWithDistances.push({ sensor, distance });

  const boundary = distance + 1;

  const { x, y } = sensor
  const top = [x, y - boundary]
  const right = [x + boundary, y]
  const bottom = [x, y + boundary]
  const left = [x - boundary, y]

  sensorBoundaries.push([
    [...top, ...right],
    [...right, ...bottom],
    [...bottom, ...left],
    [...left, ...top],
  ])
}

const intersections = []

for (const boundary1 of sensorBoundaries) {
  for (const boundary2 of sensorBoundaries) {
    for (const line1 of boundary1) {
      for (const line2 of boundary2) {
        const intersection = getLineIntersection(line1, line2)

        if (!intersection) continue

        intersections.push(intersection)
      }
    }
  }
}

let result
for (const intersection of intersections) {
  const { x, y } = intersection

  if (x < 0 || x > 4000000 || y < 0 || y > 4000000) {
    continue
  }

  let detected = false
  for (const { sensor, distance } of sensorWithDistances) {
    const intersectionDistance = getManhattanDistance(
      sensor,
      {x,y}
    )

    if (intersectionDistance <= distance) {
      detected = true
      break
    }
  }

  if (!detected) {
    result = { x, y }
    break
  }
}

console.log(result.x * 4000000 + result.y);

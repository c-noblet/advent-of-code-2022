const fs = require('fs');
const { getMin, getMax } = require('../utils/array.js');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n');

const scans = content.map(l => {
  const match = l.match(/[-0-9]+/g).map(c => parseInt(c));
  return {
    sensor: { x: match[0], y: match[1] },
    beacon: { x: match[2], y: match[3] }
  }
});

function getManhattanDistance(a, b) { 
  return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
}

const beacons = scans.map(scan => scan.beacon);
const sensors = scans.map(scan => scan.sensor);
const manhattanDistances = scans.map(scan => getManhattanDistance(scan.sensor, scan.beacon));

const mins = sensors.map((sensor, index) => sensor.x - manhattanDistances[index]);
const maxs = sensors.map((sensor, index) => sensor.x + manhattanDistances[index]);
const minX = getMin(mins);
const maxX = getMax(maxs);

function report(y) {
  let positionsCount = 0;
  for (let x = minX; x <= maxX; x++) {
    if (
      beacons.some(
        beacon => beacon.x === x && beacon.y === y
      )
    ) {
      continue;
    } else if (
      sensors.some(
        (sensor, index) => getManhattanDistance(sensor, {x, y}) <= manhattanDistances[index]
      )
    ) {
      positionsCount++;
    }
  }

  return positionsCount;
}

console.log(report(2000000));

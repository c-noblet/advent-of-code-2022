const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').split('\n').filter(v => v !== '');

const columns = [
  'MJCBFRLH',
  'ZCD',
  'HJFCNGW',
  'PJDMTSB',
  'NCDRJ',
  'WLDQPJGZ',
  'PZTFRH',
  'LVMG',
  'CBGPFQRJ'
].map(col => col.split(''));

content.forEach((row) => {
  const items = row.split(' ');
  const move = parseInt(items[1]);
  const from = parseInt(items[3]) - 1;
  const to = parseInt(items[5]) - 1;

  for (let i = 0; i < move; i++) {
    const crate = columns[from].pop();
    columns[to].push(crate);
  }
});

let topCrates = '';
columns.forEach(col => {
  topCrates += col.pop();
});
console.log(topCrates);

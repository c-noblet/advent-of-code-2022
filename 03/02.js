const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').split('\n').filter(v => v !== '');

const alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
let sum = 0;

for (let i = 1; i < content.length; i = i + 3) {
  const bags = [content[i - 1].split(''), content[i].split(''), content[i + 1].split('')];
  const result = bags.shift().filter((v) => bags.every((a) => a.indexOf(v) !== -1));
  const doubleItem = result.filter((item, pos) => result.indexOf(item) == pos)[0];
  const priority = alphabet.indexOf(doubleItem) + 1;
  sum += priority;
}
console.log(sum);


const readLine = require('readline');
const fs = require('fs');
const file = './input.txt';
const rl = readLine.createInterface({
  input : fs.createReadStream(file),
  output : process.stdout,
  terminal: false
});

const alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
let sum = 0;

rl.on('line', (line) => { 
  const compartmentA = line.slice(0,line.length/2).split('');
  const compartmentB = line.slice(line.length/2, line.length).split('');
  const doubleItems = compartmentA.filter(value => compartmentB.includes(value));
  const doubleItem = doubleItems.filter((item, pos) => doubleItems.indexOf(item) == pos)[0];
  const priority = alphabet.indexOf(doubleItem) + 1;
  sum += priority;
}).on('close', () => {
  console.log(sum);
});

const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').split('\n').filter(v => v !== '');

let sum = 0;

for (let i = 0; i < content.length; i++) {
  [firstPair, secondPair] = content[i].split(',').map(pair => pair.split('-').map(section => parseInt(section)));

  if ((firstPair[0] >= secondPair[0] && firstPair[1] <= secondPair[1]) || (secondPair[0] >= firstPair[0] && secondPair[1] <= firstPair[1])) {
    sum++;
  }  
}

console.log(sum);

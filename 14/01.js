const fs = require('fs');
const file = './input.txt';
const content = fs.readFileSync(file, 'utf-8').trim().split('\n').map(l => l.split(' -> ').map(e => {
  const arr = e.split(',');
  return { x: arr[0], y: arr[1] }
}));

class Grid {
  constructor(rocks) {
    this.setSettings(rocks);
    this.rocks = this.parseRocks(rocks);
    this.map = this.buildMap();
    this.map = this.setRocks(this.map, this.rocks);
    this.map = this.setSand(this.map);
    this.showMap();
  }
  
  setSettings(rocks) {
    this.settings = {};
    this.settings.minX = 0;
    this.settings.maxX = 0;
    this.settings.minY = 0;
    this.settings.maxY = 0;
    rocks.forEach((rockLine, rockLineKey) => {
      rockLine.forEach((rock, rockKey) => {
        if (rockLineKey == 0 && rockKey == 0) {
          this.settings.minX = parseInt(rock.x);
          this.settings.maxX = parseInt(rock.x);
          this.settings.minY = parseInt(rock.y);
          this.settings.maxY = parseInt(rock.y);
        } else {
          if (rock.x < this.settings.minX) this.settings.minX = parseInt(rock.x);
          if (rock.x > this.settings.maxX) this.settings.maxX = parseInt(rock.x);
          if (rock.y < this.settings.minY) this.settings.minY = parseInt(rock.y);
          if (rock.y > this.settings.maxY) this.settings.maxY = parseInt(rock.y);
        }
      });
    });

    this.settings.rows = this.settings.maxY + 1;
    this.settings.columns = (this.settings.maxX - this.settings.minX) + 1;

    this.settings.sandStart = this.rockToPosition({x: 500, y: 0});
  }

  parseRocks(rocks) {
    rocks.forEach((rockLine, y) => {
      rockLine.forEach((rock, x) => {
        rocks[y][x] = this.rockToPosition(rock);
      });
    });

    return rocks;
  }

  rockToPosition(rock) {
    return { x: parseInt(rock.x - this.settings.minX), y: parseInt(rock.y) };
  }

  buildMap() {
    const map = [];
    for (let i = 0; i < this.settings.rows; i++) {
      map.push([]);
      for (let j = 0; j < this.settings.columns; j++) {
        map[i].push('.');
      }
    }
    map[this.settings.sandStart.y][this.settings.sandStart.x] = '+';
    return map;
  }

  setRocks(map, rocks) {
    rocks.forEach((rockLine) => {
      for (let i = 0; i < rockLine.length - 1; i++) {
        const startX = rockLine[i].x;
        const startY = rockLine[i].y;
        const endX = rockLine[i + 1].x;
        const endY = rockLine[i + 1].y;

        if (startX === endX) {
          const start = (startY > endY) ? endY : startY;
          const end = (startY > endY) ? startY : endY;
          for (let j = start; j < end + 1; j++) {
            map[j][startX] = '#';
          }
        } else if (startY === endY) {
          const start = (startX > endX) ? endX : startX;
          const end = (startX > endX) ? startX : endX;
          for (let j = start; j < end + 1; j++) {
            map[startY][j] = '#'
          };
        }
      }
    });
    return map;
  }

  setSand(map) {
    let isDone = false;
    do {
      let sand = { ...this.settings.sandStart };
      let isFalling = true;

      if (!this.canFall(sand) && !this.canGoLeft(sand) && !this.canGoRight(sand)) {
        isDone = true;
      }

      do {
        if (
          map[sand.y + 1] == undefined ||
          map[sand.y + 1][sand.x] == undefined ||
          map[sand.y + 1][sand.x - 1] == undefined ||
          map[sand.y + 1][sand.x + 1] == undefined
        ) {
          isFalling = false;
          isDone = true;
        } else if (this.canFall(sand)) {
          sand.y++;
        } else if (this.canGoLeft(sand)) {
          sand.y++;
          sand.x--;
        } else if (this.canGoRight(sand)) {
          sand.y++;
          sand.x++;
        } else {
          map[sand.y][sand.x] = 'O';
          isFalling = false;
        }
      } while (isFalling);
    } while (!isDone);
    return map;
  }
  
  canFall(position) {
    return (
      this.map[position.y + 1][position.x] !== '#' &&
      this.map[position.y + 1][position.x] !== 'O'
    );
  }

  canGoLeft(position) {
    return (
      this.map[position.y + 1][position.x - 1] !== '#' &&
      this.map[position.y + 1][position.x - 1] !== 'O'
    );
  }

  canGoRight(position) {
    return (
      this.map[position.y + 1][position.x + 1] !== '#' &&
      this.map[position.y + 1][position.x + 1] !== 'O'
    );
  }

  showMap() {
    this.map.map(el => console.log(el.join('')));
    let count = 0;
    this.map.map(line => line.map(c => { if ('O' === c) count++; }));
    console.log(count);
  }
}

const grid = new Grid(content);
